# Ismoke
Manage your joint-session

# Description

**Features**
* On the fly new joint-session;
* Detailed new joint-session;
* Visualize your joint-session;
* Edit joint-session;
* Delete joint-session;
* Import/export joint-session functionality;
* Data are stored only locally.

# Contributing
Any kind of improvements or bugs report are welcome.

# License
Ismoke is Free Software; you can redistribute and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
