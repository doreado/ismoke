package org.doreado.ismoke;

import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        setTitle(getString(R.string.about_activity_title));
        ((TextView) findViewById(R.id.about_app_version))
                .setText(BuildConfig.VERSION_NAME);
    }


    public void viewOnGitlab(View view) {
        startActivity(new Intent(Intent.ACTION_VIEW)
                .setData(Uri.parse(getString(R.string.gitlab_link))));
    }
}