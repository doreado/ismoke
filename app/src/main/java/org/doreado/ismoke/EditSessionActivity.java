/*
 * Copyright (C) 2021 Edoardo Manfredi <edoardom59@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.doreado.ismoke;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.text.InputType;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import org.doreado.ismoke.utility.ShrPref;
import org.doreado.ismoke.utility.Utils;

import java.text.ParseException;
import java.util.Calendar;

import static java.lang.Integer.parseInt;
import static java.lang.String.valueOf;
import static org.doreado.ismoke.utility.Utils.*;

public class EditSessionActivity extends AppCompatActivity {
    private Sessions sessions;
    private Session current;
    private int position;
    private Calendar cldr = Calendar.getInstance();
    private EditText sessionDate, sessionTime, sessionNote, thisJointNumber;
    private boolean toWrite = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_and_new_session);

        position = getIntent().getIntExtra("position", -1);
        sessions = getIntent().getParcelableExtra("sessions");
        current = sessions.getSession(position);

        String[] date = stringFromDate(current.getDate(), userFormat).split(" ");

        sessionDate = findViewById(R.id.sessionDate);
        sessionTime = findViewById(R.id.sessionTime);
        sessionNote = findViewById(R.id.sessionNote);
        thisJointNumber = findViewById(R.id.this_joint_number);

        sessionDate.setText(date[0]);
        sessionTime.setText(date[1]);
        sessionNote.setText(current.getNote());
        thisJointNumber.setText(valueOf(current.getJointNumber()));

        sessionDate.setInputType(InputType.TYPE_NULL);
        sessionTime.setInputType(InputType.TYPE_NULL);

        sessionTime.setOnClickListener(v -> {
            int minute = cldr.get(Calendar.MINUTE);
            int hours = cldr.get(Calendar.HOUR_OF_DAY);

            TimePickerDialog pickerDialog = new TimePickerDialog(EditSessionActivity.this,
                    (view, hourOfDay, minute1) ->
                            sessionTime.setText(getString(R.string.time, hourOfDay, minute1)),
                    hours, minute, true);
            pickerDialog.show();
        });

        sessionDate.setOnClickListener(v -> {
            int day = cldr.get(Calendar.DAY_OF_MONTH);
            int month = cldr.get(Calendar.MONTH);
            int year = cldr.get(Calendar.YEAR);
            // date picker dialog
            DatePickerDialog picker = new DatePickerDialog(EditSessionActivity.this,
                    (view, year1, monthOfYear, dayOfMonth)
                            -> sessionDate.setText(getString(R.string.user_data,
                            dayOfMonth, monthOfYear + 1, year1)), year, month, day);
            picker.show();
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.new_session_activity_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.save_session) {
            saveSession();
        } else if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Toast.makeText(this, getString(R.string.undo_edit), Toast.LENGTH_SHORT).show();
        super.onBackPressed();
    }

    public void saveSession() {
        if (parseInt(thisJointNumber.getText().toString()) <= 0) {
            Toast.makeText(this, getString(R.string.bad_joint_number), Toast.LENGTH_SHORT).show();
            return;
        }
        if (parseInt(thisJointNumber.getText().toString()) > 420) {
            Toast.makeText(this, getString(R.string.max_joint_number_session), Toast.LENGTH_SHORT).show();
            return;
        }

        if (TextUtils.isEmpty(sessionNote.getText().toString())) {
            current.setNote(" ");
        } else sessions.getSession(position).setNote(sessionNote.getText().toString());

        try {
            sessions.getSession(position).setDate(dateFromString( sessionDate.getText().toString()
                    + " " + sessionTime.getText().toString(), userFormat));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        sessions.setSessionJointNumbers(position, parseInt(thisJointNumber.getText().toString()));

        Intent intent = new Intent(this, getCallingActivity().getClass());
        intent.putExtra("sessions", sessions);
        intent.putExtra("position", position);
        setResult(RESULT_OK, intent);

        Toast.makeText(this, "Joint Aggiornato", Toast.LENGTH_LONG).show();
        toWrite = true;
        finish();
    }

    @Override
    protected void onPause() {
        if (toWrite) {
            ShrPref shrPref = new ShrPref(this, Utils.shrPref);
            shrPref.setInt("joint_number", sessions.getJointNumber());
            shrPref.setInt("session_number", sessions.getSessionList().size());

            sessions.writeSessions(this);
        }

        super.onPause();
    }
}