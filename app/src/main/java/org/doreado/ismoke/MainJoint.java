/*
 * Copyright (C) 2021 Edoardo Manfredi <edoardom59@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.doreado.ismoke;

import android.content.Intent;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.view.*;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import org.doreado.ismoke.session.Session;
import org.doreado.ismoke.session.Sessions;
import org.doreado.ismoke.utility.ShrPref;

import java.io.*;
import java.util.Date;

import static org.doreado.ismoke.utility.ManipulationIO.*;
import static org.doreado.ismoke.utility.Utils.*;

public class MainJoint extends AppCompatActivity {
    private final int EXPORT_CSV = 0;
    private final int IMPORT_CSV = 1;
    private final int SESSIONS_ACTIVITY_CODE = 2;
    private final int NEW_SESSION_CODE = 3;
    private final int SHOW_STAT_CODE = 4;

    Toast mToast;
    private boolean toWrite = false;
    private ShrPref userData;

    Sessions sessions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setTitle(getString(R.string.app_name));

        userData = new ShrPref(this, shrPref);
        sessions = new Sessions();
        sessions.loadSessions(this, printDir(this));

        checkFirstRun();

        printSessionStats();
    }

    private void checkFirstRun() {
        final int NOT_EXIST = -1;
        final String VERSION_CODE_KEY = "version_code";

        int currentVersionCode = BuildConfig.VERSION_CODE;
        SharedPreferences preferences = getSharedPreferences(shrPref, MODE_PRIVATE);
        int savedVersionCode = preferences.getInt(VERSION_CODE_KEY, NOT_EXIST);

        if (savedVersionCode == currentVersionCode) return;
        else if (savedVersionCode == NOT_EXIST) {
            /* first run */
        } else if (savedVersionCode < currentVersionCode) {
            /* update */
        }

        preferences.edit().putInt(VERSION_CODE_KEY, currentVersionCode).apply();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_activity_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.new_session) {
            newSessionActivity();
        } else if (id == R.id.export_csv) {
            exportCSV();
        } else if (id == R.id.import_csv) {
            importCSV();
        } else if (id == R.id.reset) {
            resetSessions();
        } else if (id == R.id.about) {
            aboutActivity();
        }
        return super.onOptionsItemSelected(item);
    }
        
    private void exportCSV() {
        Intent intent = new Intent(Intent.ACTION_CREATE_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TITLE, "session_list");

        startActivityForResult(intent, EXPORT_CSV);
    }

    private void importCSV() {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("text/plain");

        startActivityForResult(intent, IMPORT_CSV);
    }

    public void writeSession(View view) {
        final EditText editText = findViewById(R.id.text_for_notes);
        String note = editText.getText().toString();
        if (TextUtils.isEmpty(note)) {
            if (mToast != null) mToast.cancel();
            mToast = Toast.makeText(this, getString(R.string.new_empty_thought), Toast.LENGTH_LONG);
            note = " ";
            mToast.show();
        }

        sessions.addSessions(new Session(new Date(), note, 1));
        printSessionStats();
        if (!toWrite) toWrite = true;

        if (mToast != null) mToast.cancel();
        mToast = Toast.makeText(this, getString(R.string.added_new_joint), Toast.LENGTH_LONG);
        mToast.show();
    }

    public void printSessionStats() {
        TextView message = findViewById(R.id.joint_counter);

        message.setText(getResources().
                getQuantityString(R.plurals.session_state,
                        sessions.getSessionList().size(), sessions.getJointNumber(),
                        sessions.getSessionList().size()));
    }

    public void showSessions(View view){
        if (sessions.getSessionList().size() <= 0)  {
            if (mToast != null) mToast.cancel();
            mToast = Toast.makeText(this, getString(R.string.none_joint), Toast.LENGTH_LONG);
            mToast.show();

            return;
        }

        Intent intent = new Intent(this, SessionsActivity.class);
        intent.putExtra("sessions", sessions);
        startActivityForResult(intent, SESSIONS_ACTIVITY_CODE);
    }

    private void aboutActivity() {
        startActivity(new Intent(this, AboutActivity.class));
    }

    public void resetSessions() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.alert_title));
        builder.setMessage(getString(R.string.alert_question));
        builder.setCancelable(true);

        builder.setPositiveButton(getString(R.string.yes),
                (dialog, which) -> {
                    File file = new File(printDir(MainJoint.this));
                    if (file.exists()) file.delete();

                    dialog.cancel();

                    sessions.getSessionList().clear();
                    sessions.setJointNumber(0);

                    printSessionStats();
                    if (!toWrite) toWrite = true;

                    if (mToast != null) mToast.cancel();
                    mToast = Toast.makeText(MainJoint.this,
                            getString(R.string.operation_completed), Toast.LENGTH_LONG);
                    mToast.show();
                });

        builder.setNegativeButton(getString(R.string.no),
                (dialog, which) -> {
                    if (mToast != null) mToast.cancel();
                    mToast = Toast.makeText(MainJoint.this,
                            getString(R.string.undo_operation), Toast.LENGTH_LONG);
                    mToast.show();

                    dialog.cancel();
                });

        AlertDialog alert = builder.create();
        alert.show();
    }

    public void newSessionActivity() {
        Intent intent = new Intent(this, NewSessionActivity.class);
        intent.putExtra("sessions", sessions);
        startActivityForResult(intent, NEW_SESSION_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data == null) return;

        switch (requestCode) {
            case SESSIONS_ACTIVITY_CODE:
            case NEW_SESSION_CODE:
            case SHOW_STAT_CODE:
                if (resultCode == RESULT_OK) {
                    sessions = data.getParcelableExtra("sessions");
                    printSessionStats();
                }
                break;
            case EXPORT_CSV:
                if (resultCode == RESULT_OK) {
                    // Example file to copy
                    File sourceFile = new File(printDir(this));

                    // InputStream constructor takes File, String (path), or FileDescriptor
                    try {
                        InputStream is = new FileInputStream(sourceFile);
                        OutputStream os = getContentResolver().openOutputStream(data.getData());
                        copy(is, os);

                        is.close();
                        os.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                break;
            case IMPORT_CSV:
                if (resultCode == RESULT_OK) {
                    String tmpFile = getCacheDir() + "/tmp";

                    // InputStream constructor takes File, String (path), or FileDescriptor
                    try {
                        int count = 0;
                        InputStream is = getContentResolver().openInputStream(data.getData());
                        OutputStream os = new FileOutputStream(tmpFile);
                        copy(is, os);

                        is.close();
                        os.close();

                        Sessions tmpSession = (new Sessions());
                        tmpSession.loadSessions(this, tmpFile);

                        new File(tmpFile).delete();
                        for (Session s : tmpSession.getSessionList()) {
                            /* maybe it's not necessary  */
                            if (!sessions.getSessionList().contains(s)) {
                               sessions.addSessions(s);
                               count++;
                            }
                        }

                        Toast.makeText(this, getResources()
                                .getQuantityString(R.plurals.sessions_loaded,
                                        count, count), Toast.LENGTH_SHORT).show();

                        if (count > 0) {
                            printSessionStats();
                            if (!toWrite) toWrite = true;
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                break;
            default:
        }
    }

    @Override
    protected void onPause() {
        if (toWrite) {
            sessions.writeSessions(this);
            userData.setInt("joint_number", sessions.getJointNumber());
            userData.setInt("session_number", sessions.getSessionList().size());
            userData.apply();
        }
        super.onPause();
    }

    public void showStat(View view) {
        if (sessions.getSessionList().size() <= 0) {
            if (mToast != null) mToast.cancel();
            mToast = Toast.makeText(this, getString(R.string.none_joint), Toast.LENGTH_LONG);
            mToast.show();

            return;
        }
        Intent i = new Intent(this, StatActivity.class);
        i.putExtra("sessions", sessions);
        startActivityForResult(i, SHOW_STAT_CODE);
    }
}