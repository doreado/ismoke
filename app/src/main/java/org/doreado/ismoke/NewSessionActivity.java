/*
 * Copyright (C) 2021 Edoardo Manfredi <edoardom59@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.doreado.ismoke;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.text.InputType;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import org.doreado.ismoke.utility.ShrPref;
import org.doreado.ismoke.utility.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import static java.lang.Integer.parseInt;
import static org.doreado.ismoke.utility.Utils.*;

public class NewSessionActivity extends AppCompatActivity {
    private EditText mNote, mTime, mDate, mJointNumber;
    private Sessions sessions;
    private boolean toWrite = false;
    private int thisJointNumber;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_and_new_session);
        final Calendar cldr = Calendar.getInstance();


        SimpleDateFormat dataFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());

        mNote = findViewById(R.id.sessionNote);
        mTime = findViewById(R.id.sessionTime);
        mJointNumber = findViewById(R.id.this_joint_number);

        mTime.setInputType(InputType.TYPE_NULL);
        mTime.setText(timeFormat.format(Calendar.getInstance().getTime()));
        mJointNumber.setText("1");
        mTime.setOnClickListener(v -> {
            int minute = cldr.get(Calendar.MINUTE);
            int hours = cldr.get(Calendar.HOUR_OF_DAY);

            TimePickerDialog pickerDialog = new TimePickerDialog(NewSessionActivity.this,
                    (view, hourOfDay, minute1) ->
                            mTime.setText(getString(R.string.time, hourOfDay, minute1)),
                    hours, minute, true);
            pickerDialog.show();
        });
                
        mDate = findViewById(R.id.sessionDate);
        mDate.setInputType(InputType.TYPE_NULL);
        mDate.setText(dataFormat.format(Calendar.getInstance().getTime()));
        mDate.setOnClickListener(v -> {
            int day = cldr.get(Calendar.DAY_OF_MONTH);
            int month = cldr.get(Calendar.MONTH);
            int year = cldr.get(Calendar.YEAR);
            // date picker dialog
            DatePickerDialog picker = new DatePickerDialog(NewSessionActivity.this,
                    (view, year1, monthOfYear, dayOfMonth) -> mDate.setText(getString(R.string.user_data,
                            dayOfMonth, monthOfYear + 1, year1)), year, month, day);
            picker.show();
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.new_session_activity_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.save_session) {
            saveSession();
        }
        return super.onOptionsItemSelected(item);
    }

    public void saveSession() {
        thisJointNumber = parseInt(mJointNumber.getText().toString());
        if (thisJointNumber <= 0) {
            Toast.makeText(this, getString(R.string.bad_joint_number), Toast.LENGTH_SHORT).show();
            return;
        }
        if (thisJointNumber > 420) {
            Toast.makeText(this, getString(R.string.max_joint_number_session), Toast.LENGTH_SHORT).show();
            return;
        }

        sessions = getIntent().getParcelableExtra("sessions");

        if (TextUtils.isEmpty(mNote.getText().toString()))
            mNote.setText(" ");
        try {
            sessions.addSessions(new Session(dateFromString(
                    mDate.getText().toString() +
                            " " + mTime.getText().toString(), userFormat),
                    mNote.getText().toString(),
                    thisJointNumber));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Toast.makeText(this, getString(R.string.added_new_joint), Toast.LENGTH_LONG).show();

        Intent intent = new Intent(this, getCallingActivity().getClass());
        intent.putExtra("sessions", sessions);
        setResult(RESULT_OK, intent);

        finish();
    }

    @Override
    protected void onPause() {
        if (toWrite) {
            ShrPref shrPref = new ShrPref(this, Utils.shrPref);
            shrPref.setInt("joint_number", shrPref.getInt("joint_number") + thisJointNumber);
            shrPref.setInt("session_number", shrPref.getInt("session_number") + 1);
            shrPref.apply();

            sessions.writeSessions(this);
        }
        super.onPause();
    }
}