/*
 * Copyright (C) 2021 Edoardo Manfredi <edoardom59@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.doreado.ismoke;

import java.util.Date;

import static java.lang.String.valueOf;
import static org.doreado.ismoke.utility.Utils.*;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import org.doreado.ismoke.utility.Utils;

public class Session implements Comparable<Session>, Parcelable {
   public static final int NUM_FIELDS = 3;

   private String note;
   private Date date;
   private int jointNumber;

   public Session(Parcel in) {
      date = new Date(in.readLong());
      note = in.readString();
      jointNumber = in.readInt();
   }

   public Session(Date date, String note, int jointNumber) {
      this.date = date;
      this.note = note;
      this.jointNumber = jointNumber;
   }

   public static final Creator<Session> CREATOR = new Creator<Session>() {
      @Override
      public Session createFromParcel(Parcel in) {
         return new Session(in);
      }

      @Override
      public Session[] newArray(int size) {
         return new Session[size];
      }
   };

   public void setJointNumber(int jointNumber) {
      this.jointNumber = jointNumber;
   }

   public void setDate(Date date) {
      this.date = date;
   }

   public void setNote(String note){
      this.note = note;
   }

   public Date getDate() {
      return date;
   }

   public String getNote() {
      return note;
   }

   public int getJointNumber() {
      return jointNumber;
   }
   public String[] toCSVStringArray() {
      return new String[] {Utils.stringFromDate(getDate(), csvFormat), getNote(), valueOf(jointNumber)};
   }

   @NonNull
   @Override
   public String toString() {
      return getDate() + "," + note + "," + jointNumber;
   }

   @Override
   public int compareTo(Session session) {
      return this.getDate().compareTo(session.date);
   }

   /* it's not really clever */
   @Override
   public boolean equals(Object obj) {
      return this.date.compareTo(((Session)obj).date) == 0
              && this.note.compareTo(((Session)obj).note) == 0;
   }

   @Override
   public int describeContents() {
      return 0;
   }

   @Override
   public void writeToParcel(Parcel dest, int flags) {
      dest.writeLong(date.getTime());
      dest.writeString(note);
      dest.writeInt(jointNumber);
   }


}