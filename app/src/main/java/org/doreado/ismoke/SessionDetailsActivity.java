/*
 * Copyright (C) 2021 Edoardo Manfredi <edoardom59@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.doreado.ismoke;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import org.doreado.ismoke.utility.ShrPref;
import org.doreado.ismoke.utility.Utils;

import static java.lang.String.valueOf;
import static org.doreado.ismoke.utility.ManipulationIO.printDir;
import static org.doreado.ismoke.utility.Utils.*;

public class SessionDetailsActivity extends AppCompatActivity {
    private int position;
    private Session session = null;
    private boolean toWrite = false;
    private final int EDIT_SESSION_CODE = 1;
    private Sessions sessions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_session_details);

        setTitle("");

        position = getIntent().getIntExtra("position", -1);
        sessions = getIntent().getParcelableExtra("sessions");
        session = sessions.getSession(position);

        if(session != null) {
            String[] date = stringFromDate(session.getDate(), userFormat).split(" ");
            ((TextView) findViewById(R.id.sessionDate)).setText(date[0]);
            ((TextView) findViewById(R.id.sessionTime)).setText(date[1]);
            ((TextView) findViewById(R.id.sessionNote)).setText(session.getNote());
            ((TextView) findViewById(R.id.this_joint_number)).setText(valueOf(session.getJointNumber()));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.session_details_activity, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.edit) {
            Intent intent = new Intent(this, EditSessionActivity.class);
            intent.putExtra("sessions", sessions);
            intent.putExtra("position", position);

            startActivityForResult(intent, EDIT_SESSION_CODE);
        } else if (item.getItemId() == R.id.delete) {
            sessions.removeSession(position);
            toWrite = true;
            Intent returnIntent = new Intent();
            returnIntent.putExtra("sessions", sessions);
            setResult(RESULT_OK, returnIntent);

            finish();
        } else if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent();
        // returnIntent.putParcelableArrayListExtra("session_list", sessionList);
        returnIntent.putExtra("sessions", sessions);
        setResult(RESULT_OK, returnIntent);

        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (data == null) return;

        if (requestCode == EDIT_SESSION_CODE) {
            if (resultCode == RESULT_OK) {
                sessions = data.getParcelableExtra("sessions");
                session = sessions.getSessionList().get(position);

                String[] date = stringFromDate(session.getDate(), userFormat).split(" ");

                ((TextView) findViewById(R.id.sessionDate)).setText(date[0]);
                ((TextView) findViewById(R.id.sessionTime)).setText(date[1]);
                ((TextView) findViewById(R.id.sessionNote)).setText(session.getNote());
                ((TextView) findViewById(R.id.this_joint_number)).setText(valueOf(session.getJointNumber()));
                toWrite = true;
            } 
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (toWrite) {
            ShrPref shrPref = new ShrPref(this, Utils.shrPref);
            shrPref.setInt("joint_number", sessions.getJointNumber());
            shrPref.setInt("session_number", sessions.getSessionList().size());
            shrPref.apply();

            sessions.loadSessions(this, printDir(this));
        }
    }
}