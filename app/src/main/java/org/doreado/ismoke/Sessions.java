package org.doreado.ismoke;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import com.opencsv.exceptions.CsvValidationException;
import org.doreado.ismoke.utility.ShrPref;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Integer.parseInt;
import static org.doreado.ismoke.Session.NUM_FIELDS;
import static org.doreado.ismoke.utility.ManipulationIO.printDir;
import static org.doreado.ismoke.utility.Utils.*;

public class Sessions implements Parcelable {
    private List<Session> sessionList;
    private int jointNumber;

    public Sessions() {
        sessionList = new ArrayList<>();
    }

    protected Sessions(Parcel in) {
        sessionList = in.createTypedArrayList(Session.CREATOR);
        jointNumber = in.readInt();
    }

    public static final Creator<Sessions> CREATOR = new Creator<Sessions>() {
        @Override
        public Sessions createFromParcel(Parcel in) {
            return new Sessions(in);
        }

        @Override
        public Sessions[] newArray(int size) {
            return new Sessions[size];
        }
    };

    public void addSessions(Session s) {
        this.getSessionList().add(s);
        jointNumber += s.getJointNumber();
    }

    public synchronized void loadSessions(Context ctx, String filePath) {
        try {
            File file = new File(filePath);
            if (!file.exists()) file.createNewFile();

            CSVReader csvReader = new CSVReader(new FileReader(file));

            String[] nextLine;
            while ((nextLine = csvReader.readNext()) != null) {
                if (nextLine.length < NUM_FIELDS) {
                    sessionList.add(new Session(dateFromString(nextLine[0], csvFormat),
                            nextLine[1], 1));
                } else sessionList.add(new Session(dateFromString(nextLine[0], csvFormat),
                        nextLine[1], parseInt(nextLine[2])));
            }

            csvReader.close();
        } catch (IOException e) {
            e.printStackTrace();
            //Toast.makeText(ctx, "IO exception", Toast.LENGTH_SHORT).show();
        }
        catch (CsvValidationException e) {
            //Toast.makeText(ctx, "Problem occurs", Toast.LENGTH_SHORT).show();
        } catch (ParseException e) {
          //  Toast.makeText(ctx, "Ho avuto problemi nella lettura del file\n" +
          //                  "Assicurati che il formato sia \"yyyy-mm-dd hh:mm\",\"pensiero\",\"numero joint\"",
          //          Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }

        jointNumber = (new ShrPref(ctx, shrPref)).getInt("joint_number");
    }

    public void writeSessions(Context ctx) {
        try {
            CSVWriter csvWriter = new CSVWriter(new FileWriter(printDir(ctx)));
            for (Session s : sessionList) {
                csvWriter.writeNext(s.toCSVStringArray());
            }

            csvWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setJointNumber(int jointNumber) {
        this.jointNumber = jointNumber;
    }

    public int getJointNumber() {
        return jointNumber;
    }

    public List<Session> getSessionList() {
        return sessionList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(sessionList);
        dest.writeInt(jointNumber);
    }

    public Session getSession(int position) {
        return this.getSessionList().get(position);
    }

    public void removeSession(int position) {
        jointNumber -= getSessionList().get(position).getJointNumber();
        this.getSessionList().remove(position);
    }

    public void setSessionJointNumbers(int position, int sessionJointNumber) {
        jointNumber += sessionJointNumber - sessionList.get(position).getJointNumber();
        sessionList.get(position).setJointNumber(sessionJointNumber);
    }
}
