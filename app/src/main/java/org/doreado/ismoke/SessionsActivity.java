/*
 * Copyright (C) 2021 Edoardo Manfredi <edoardom59@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.doreado.ismoke;

import org.doreado.ismoke.adapter.RecyclerTouchListener;
import org.doreado.ismoke.adapter.SessionAdapter;
import android.content.Intent;
import android.view.*;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import org.doreado.ismoke.utility.ShrPref;
import org.doreado.ismoke.utility.Utils;

import java.util.List;

public class SessionsActivity extends AppCompatActivity implements SessionAdapter.SessionClickListener{
    private static final int DETAIL_ACTIVITY_CODE = 1;
    private static final int EDIT_ACTIVITY_CODE = 2;

    private boolean toWrite;
    private SessionAdapter adapter;
    RecyclerView recyclerView;
    private Toast mToast;

    Sessions sessions;
    private List<Session> sessionList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sessions);

        toWrite = false;

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(SessionsActivity.this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        sessions = getIntent().getParcelableExtra("sessions");
        sessionList = sessions.getSessionList();
        adapter = new SessionAdapter(sessionList, this);
        recyclerView.setAdapter(adapter);
        registerForContextMenu(recyclerView);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView,
                (view, position) -> showSessionDetails(sessionList, position)));
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.session_item_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        int position, id = item.getItemId();

        try {
            position = adapter.getPosition();
        } catch (Exception e) {
            Toast.makeText(this, "impossibile aprie il menu", Toast.LENGTH_SHORT).show();
            return super.onContextItemSelected(item);
        }
        if (id == R.id.visualize) {
            showSessionDetails(sessionList, position);
        } else if (id == R.id.edit) {
            Intent intent = new Intent(this, EditSessionActivity.class);
            intent.putExtra("sessions", sessions);
            intent.putExtra("position", position);
            startActivityForResult(intent, EDIT_ACTIVITY_CODE);
        } else if(id == R.id.delete) {
                sessions.setJointNumber(sessions.getJointNumber() - sessionList.get(position).getJointNumber());
                sessionList.remove(position);
                toWrite = true;
                adapter.notifyItemRemoved(position);
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent output = new Intent();
        output.putExtra("sessions", sessions);

        setResult(RESULT_OK, output);

        super.onBackPressed();
    }

    public void showSessionDetails(List<Session> list, int position) {
        Intent intent = new Intent(this, SessionDetailsActivity.class);
        intent.putExtra("sessions", sessions);
        intent.putExtra("position", position);

        startActivityForResult(intent, DETAIL_ACTIVITY_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (data == null) return;
        if (requestCode == DETAIL_ACTIVITY_CODE) {
            if (resultCode == RESULT_OK) {
                sessions = data.getParcelableExtra("sessions");
                adapter = new SessionAdapter(sessions.getSessionList(), this);
                recyclerView.setAdapter(adapter);
                registerForContextMenu(recyclerView);
          }
        }
        
        if (requestCode == EDIT_ACTIVITY_CODE) {
            if (resultCode == RESULT_OK) toWrite = true;
            else Toast.makeText(this, "modifiche annullate", Toast.LENGTH_SHORT).show();

            sessions = data.getParcelableExtra("sessions");
            adapter = new SessionAdapter(sessions.getSessionList(), this);
            recyclerView.setAdapter(adapter);
            registerForContextMenu(recyclerView);
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (toWrite) {
            ShrPref shrPref = new ShrPref(this, Utils.shrPref);
            shrPref.setInt("joint_number", sessions.getJointNumber());
            shrPref.setInt("session_number", sessionList.size());
            shrPref.apply();

            sessions.writeSessions(this);
        }
    }
}