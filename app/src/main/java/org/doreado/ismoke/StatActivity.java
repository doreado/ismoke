package org.doreado.ismoke;

import android.content.Intent;
import android.os.Bundle;

import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.*;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class StatActivity extends AppCompatActivity {
    CheckBox[] checkBoxWeekDay = new CheckBox[4];
    CheckBox[] checkBoxHours = new CheckBox[4];

    LineChart chartHours;
    LineChart chartWeekDay;

    LineDataSet[] weekDaySet = new LineDataSet[4];
    LineDataSet[] hoursSet = new LineDataSet[4];

    Sessions sessions;
    List<Session> sessionList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stat);
        setTitle(getString(R.string.activity_stat_name));

        sessions = getIntent().getParcelableExtra("sessions");
        sessionList = sessions.getSessionList();

        checkBoxWeekDay[0] = findViewById(R.id.stat_always_week_day);
        checkBoxWeekDay[1] = findViewById(R.id.stat_this_year_week_day);
        checkBoxWeekDay[2] = findViewById(R.id.stat_this_month_week_day);
        checkBoxWeekDay[3] = findViewById(R.id.stat_this_week_day);

        checkBoxHours[0] = findViewById(R.id.stat_always_hours);
        checkBoxHours[1] = findViewById(R.id.stat_this_year_hours);
        checkBoxHours[2] = findViewById(R.id.stat_this_month_hours);
        checkBoxHours[3] = findViewById(R.id.stat_this_week_hours);

        for (int i = 0; i < 4; i++) {
            checkBoxWeekDay[i].setChecked(true);
            checkBoxHours[i].setChecked(true);
        }

        chartHours = findViewById(R.id.char_joint_over_hours);
        chartHours.getLegend().setEnabled(false);
        chartHours.setDescription(null);
        chartHours.getXAxis().setAxisMaximum(0f);
        chartHours.getXAxis().setAxisMaximum(23f);
        chartHours.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        chartHours.getXAxis().setGranularity(1f);
        chartHours.getAxisLeft().setAxisMinimum(0f);
        chartHours.getAxisLeft().setGranularity(1f);
        chartHours.getAxisRight().setGranularity(1f);

        chartWeekDay = findViewById(R.id.chart);
        chartWeekDay.getLegend().setEnabled(false);
        chartWeekDay.setDescription(null);
        chartWeekDay.getXAxis().setAxisMaximum(0f);
        chartWeekDay.getXAxis().setAxisMaximum(7f);
        chartWeekDay.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        chartWeekDay.getXAxis().setGranularity(1f);
        chartWeekDay.getAxisLeft().setGranularity(1f);
        chartWeekDay.getAxisLeft().setAxisMinimum(0f);
        chartWeekDay.getAxisRight().setGranularity(1f);
        final String[] weekDay = new String[] { " ", "Lun", "Mar", "Mer", "Gio",
                "Ven", "Sab","Dom" };
        chartWeekDay.getXAxis().setValueFormatter(new IndexAxisValueFormatter(weekDay));

        jointOverWeekDayLine();
        jointOverHoursLine();
    }


    public void jointOverHoursLine() {
        List<Entry>[] entries = new List[4];
        for (int i = 0; i < 4; i++) {
            entries[i] = new ArrayList<>();
        }

        Calendar c = Calendar.getInstance();
        Calendar now = Calendar.getInstance();
        now.setTime(new Date());

        int[][] jointOverTime = new int[4][24];

        for (Session s: sessionList) {
            c.setTime(s.getDate());

            int jointNumber = s.getJointNumber();
            int x = c.get(Calendar.HOUR_OF_DAY);

            jointOverTime[0][x] += jointNumber;
            if (c.get(Calendar.YEAR) == now.get(Calendar.YEAR)) {
                jointOverTime[1][x] += jointNumber;
                if (c.get(Calendar.MONTH) == now.get(Calendar.MONTH)) {
                    jointOverTime[2][x] += jointNumber;
                    if (c.get(Calendar.WEEK_OF_YEAR) == c.get(Calendar.WEEK_OF_YEAR)) {
                        jointOverTime[3][x] += jointNumber;
                    }
                }
            }
        }

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 24; j++) {
                entries[i].add(new Entry(j, jointOverTime[i][j]));
            }
        }

        for (int i = 0; i < 4; i++) {
            hoursSet[i] = new LineDataSet(entries[i], "");
            hoursSet[i].setDrawValues(false);
            hoursSet[i].setLineWidth(2f);
        }

        hoursSet[0].setColors(new int[]{R.color.colorAllStat}, this);
        hoursSet[1].setColors(new int[]{R.color.colorYearStat}, this);
        hoursSet[2].setColors(new int[]{R.color.colorMonthStat}, this);
        hoursSet[3].setColors(new int[]{R.color.colorWeekStat}, this);

        chartHours.setData(new LineData(hoursSet));
        chartHours.invalidate();
    }

    public void jointOverWeekDayLine() {
        List<Entry>[] entries = new List[4];
        for (int i = 0; i < 4; i++) {
            entries[i] = new ArrayList<>();
        }

        Calendar c = Calendar.getInstance();
        Calendar now = Calendar.getInstance();
        now.setTime(new Date());

        // [0][] -> all
        // [1][] -> yearly
        // [2][] -> montly
        // [3][] -> weekly
        int[][] jointOverDay = new int[4][7];
        for (Session s: sessionList) {
            c.setTime(s.getDate());
            int x = c.get(Calendar.DAY_OF_WEEK) - 1;
            int jointNumber = s.getJointNumber();

            jointOverDay[0][x] += jointNumber;
            if (c.get(Calendar.YEAR) == now.get(Calendar.YEAR)) {
                jointOverDay[1][x] += jointNumber;
                if (c.get(Calendar.MONTH) == now.get(Calendar.MONTH)) {
                    jointOverDay[2][x] += jointNumber;
                    if (c.get(Calendar.WEEK_OF_YEAR) == c.get(Calendar.WEEK_OF_YEAR)) {
                        jointOverDay[3][x] += jointNumber;
                    }
                }
            }
        }

        for (int i = 0; i < 4; i++) {
            for (int j = 1; j != 7; j++) {
                entries[i].add(new Entry(j, jointOverDay[i][j]));
            }
            entries[i].add(new Entry(7, jointOverDay[i][0]));
        }

        for (int i = 0; i < 4; i++) {
            weekDaySet[i] = new LineDataSet(entries[i], "");
            weekDaySet[i].setDrawValues(false);
            weekDaySet[i].setLineWidth(2f);
        }

        weekDaySet[0].setColors(new int[]{R.color.colorAllStat}, this);
        weekDaySet[1].setColors(new int[]{R.color.colorYearStat}, this);
        weekDaySet[2].setColors(new int[]{R.color.colorMonthStat}, this);
        weekDaySet[3].setColors(new int[]{R.color.colorWeekStat}, this);

        chartWeekDay.setData(new LineData(weekDaySet));
        chartWeekDay.invalidate();
    }

    public void onClickCheck(View view) {
        boolean checked = ((CheckBox) view).isChecked();
        int id = view.getId();
        if (id == R.id.stat_always_week_day) {
            weekDaySet[0].setVisible(checked);
        } else if (id == R.id.stat_this_year_week_day) {
            weekDaySet[1].setVisible(checked);
        } else if (id == R.id.stat_this_month_week_day) {
            weekDaySet[2].setVisible(checked);
        } else if (id == R.id.stat_this_week_day) {
            weekDaySet[3].setVisible(checked);
        } else if (id == R.id.stat_always_hours) {
            hoursSet[0].setVisible(checked);
        } else if (id == R.id.stat_this_year_hours) {
            hoursSet[1].setVisible(checked);
        } else if (id == R.id.stat_this_month_hours) {
            hoursSet[2].setVisible(checked);
        } else if (id == R.id.stat_this_week_hours) {
            hoursSet[3].setVisible(checked);
        }

        chartWeekDay.invalidate();
        chartHours.invalidate();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("sessions", sessions);
        setResult(RESULT_OK, returnIntent);

        super.onBackPressed();
    }
}