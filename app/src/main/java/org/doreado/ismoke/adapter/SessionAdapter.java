/*
 * Copyright (C) 2021 Edoardo Manfredi <edoardom59@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.doreado.ismoke.adapter;

import android.content.Context;
import android.view.*;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import org.doreado.ismoke.R;
import org.doreado.ismoke.Session;
import org.doreado.ismoke.utility.Utils;

import java.util.List;

public class SessionAdapter extends RecyclerView.Adapter<SessionAdapter.SessionViewHolder> {

    private List<Session> list;
    Context context;
    private int position;

    public class SessionViewHolder extends RecyclerView.ViewHolder {

        TextView sessionDate;
        TextView sessionNote;
        TextView sessionTime;

        public int index = 0;
        View view;

        public SessionViewHolder(View view) {
            super(view);
            sessionNote = view.findViewById(R.id.sessionNote);
            sessionDate = view.findViewById(R.id.sessionDate);
            sessionTime = view.findViewById(R.id.sessionTime);
        }
    }
    

    public interface SessionClickListener {
    }

    public SessionAdapter(List<Session> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public SessionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View sessionView= inflater.inflate(R.layout.list_item, parent, false);

        return new SessionViewHolder(sessionView);
    }


    @Override
    public void onBindViewHolder(SessionViewHolder viewHolder, int position) {
        Session session = list.get(position);
        viewHolder.index = viewHolder.getBindingAdapterPosition();
        String[] date = Utils.stringFromDate(session.getDate(), Utils.userFormat).split(" ");

        viewHolder.sessionDate.setText(date[0]);
        viewHolder.sessionTime.setText(date[1]);
        viewHolder.sessionNote.setText(session.getNote());

        viewHolder.itemView.setOnLongClickListener(v -> {
            setPosition(viewHolder.getBindingAdapterPosition());
            return false;
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}