/*
 * Copyright (C) 2021 Edoardo Manfredi <edoardom59@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.doreado.ismoke.utility;

import android.content.Context;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;

import org.doreado.ismoke.Session;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import com.opencsv.exceptions.CsvValidationException;

import java.io.*;
import java.text.ParseException;
import java.util.*;

import static java.lang.Integer.parseInt;
import static org.doreado.ismoke.Session.NUM_FIELDS;
import static org.doreado.ismoke.utility.Utils.*;

public class ManipulationIO extends AppCompatActivity {
    private static final String userCSVFile = "userSmoke.csv";

    public static String printDir(Context ctx) {
        return ctx.getFilesDir() + "/" + userCSVFile;
    }

    public static boolean writeSessionCSV(Context ctx, Session session) {
        try {
            File file = new File(ctx.getFilesDir(), userCSVFile);
            if (!file.exists()) {
                file.createNewFile();
            }

            FileWriter fileWriter = new FileWriter(file.getAbsoluteFile(), true);

            CSVWriter csvWriter = new CSVWriter(fileWriter);
            csvWriter.writeNext(session.toCSVStringArray());
            csvWriter.close();
            
            return true;
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        
        return false;
    }

    public static synchronized ArrayList<Session> readCSVToList(Context ctx, String filePath) {
        ArrayList<Session> list = new ArrayList<Session>();
        try {
            File file = new File(filePath);
            if (!file.exists()) file.createNewFile();

            CSVReader csvReader = new CSVReader(new FileReader(file));

            String[] nextLine;
            while ((nextLine = csvReader.readNext()) != null) {
                if (nextLine.length < NUM_FIELDS) {
                    list.add(new Session(dateFromString(nextLine[0], csvFormat),
                            nextLine[1], 1));
                } else list.add(new Session(dateFromString(nextLine[0], csvFormat),
                            nextLine[1], parseInt(nextLine[2])));
            }

            csvReader.close();
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(ctx, "IO exception", Toast.LENGTH_SHORT).show();
        }
        catch (CsvValidationException e) {
            Toast.makeText(ctx, "Problem occurs", Toast.LENGTH_SHORT).show();
        } catch (ParseException e) {
            Toast.makeText(ctx, "Ho avuto problemi nella lettura del file\n" +
                    "Assicurati che il formato sia \"yyyy-mm-dd hh:mm\",\"pensiero\",\"numero joint\"",
                    Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }

        return list;
    }

    public static ArrayList<Session> readCSVToList(Context ctx) {
        return readCSVToList(ctx, ctx.getFilesDir() + "/" + userCSVFile);
    }

    public static void writeCSVFromList(Context ctx, List<Session> list) {
        try {
            FileWriter fileWriter = new FileWriter(printDir(ctx));
            CSVWriter csvWriter = new CSVWriter(fileWriter);
            for (Session s : list) {
                csvWriter.writeNext(s.toCSVStringArray());
            }

            csvWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static boolean deleteByIndex(Context ctx, ArrayList<Session> list, int position) {
        if (position < 0) return false;
        list.remove(position);
        writeCSVFromList(ctx, list);
        decSessionNumber(ctx);
        return true;
    }

    public static void copy(InputStream in, OutputStream out) {
        try {
            byte[] buffer = new byte[1024];
            int length;
            while ((length = in.read(buffer)) > 0) {
                out.write(buffer, 0, length);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                //
            }
        }
    }

    public static void copy(File src, File dst) throws IOException {
        try (InputStream in = new FileInputStream(src)) {
            try (OutputStream out = new FileOutputStream(dst)) {
                // Transfer bytes from in to out
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
            }
        }
    }
}