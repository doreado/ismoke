package org.doreado.ismoke.utility;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

public class ShrPref {
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    public ShrPref(Context context, String name) {
        if (context != null)
            sharedPreferences = context.getSharedPreferences(name, Context.MODE_PRIVATE);
    }

    public int getInt(String key) {
       return sharedPreferences.getInt(key, -1);
    }

    @SuppressLint("CommitPrefEdits")
    public void setInt(String key, int value) {
        if (editor == null)
            editor = sharedPreferences.edit();

        editor.putInt(key, value);
    }

    public void apply() {
        if (editor != null)
            editor.apply();
    }
}