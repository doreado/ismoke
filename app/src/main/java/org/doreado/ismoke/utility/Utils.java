/*
 * Copyright (C) 2021 Edoardo Manfredi <edoardom59@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.doreado.ismoke.utility;

import android.content.Context;
import android.content.SharedPreferences;
import org.doreado.ismoke.Session;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Utils {
    public static String shrPref = "info";
    public final static String SESSION_NUMBER_KEY = "session_number";
    public final static String JOINT_NUMBER_KEY = "session_number";

    public static String csvFormat = "yyyy-MM-dd HH:mm";
    public static String userFormat = "dd/MM/yyyy HH:mm";

    public static int getJointNumber(ArrayList<Session> list) {
        int num = 0;
        for(Session s : list) {
            num += s.getJointNumber();
        }

        return num;
    }

    public static int getJointNumber(Context ctx) {
        return ctx.getSharedPreferences(shrPref, Context.MODE_PRIVATE).getInt(JOINT_NUMBER_KEY, -1);
    }
    public static int getSessionNumber(Context ctx){
        return ctx.getSharedPreferences(shrPref, Context.MODE_PRIVATE).getInt(SESSION_NUMBER_KEY, -1);
    }

    public static void incSessionNumber(Context ctx) {
        updateSharedPreferences(ctx, SESSION_NUMBER_KEY, getSessionNumber(ctx) + 1);
    }

    public static void decSessionNumber(Context ctx) {
        updateSharedPreferences(ctx, SESSION_NUMBER_KEY, getSessionNumber(ctx) - 1);
    }

    public  static  void updateSharedPreferences(Context ctx, String fields, int value) {
        SharedPreferences sharedPreferences = ctx.getSharedPreferences(shrPref, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(fields, value);
        editor.apply();
    }

    public static void updateSharedPreferences(Context ctx, String fields, String value) {
        SharedPreferences sharedPreferences = ctx.getSharedPreferences(shrPref, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(fields, value);
        editor.apply();
    }
    
    public static String stringFromDate(Date date, String pattern) {
       SimpleDateFormat formatter = new SimpleDateFormat(pattern);
       return formatter.format(date);
    }

    public static Date dateFromString(String stringDate, String pattern) throws ParseException{
        return new SimpleDateFormat(pattern).parse(stringDate);
    }
}
